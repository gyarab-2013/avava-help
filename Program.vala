using Curses;

public class Program : GLib.Object
{	
	public static void main()
	{
		Intl.setlocale(LocaleCategory.CTYPE, "");
		
		initscr();

		SetColours();
		ClearScreen();

		Terminal mainTerminal = new Terminal(COLS - 8, LINES - 8, 4, 4, "Vítejte - Avava 2");
		mainTerminal.AddControl(new Label(mainTerminal, 0, 1, "Vítejte na studentském serveru Avava2!"));
		mainTerminal.AddControl(new Label(mainTerminal, 0, 2, "Avava 2 běží na Linuxu, konkrétně Debianu"));
		mainTerminal.AddControl(new Label(mainTerminal, 0, 4, "Pokud jste již zkušení s Linuxem, nebo jiným UNIXovým systém, můžete se rovnou vrhnout do práce."));
		mainTerminal.AddControl(new Label(mainTerminal, 0, 5, "Jestli ne, tak to vůbec nevadí, protože na internetu je spostu příruček. Google je váš přitel."));
		mainTerminal.AddControl(new Label(mainTerminal, 0, 7, "Na Avavě je nainstalována široká škála runtimů, programů, a nástrojů proveškeré vaše projekty."));
		mainTerminal.AddControl(new Label(mainTerminal, 0, 8, "Jako například: Java, Mono, Python / Django, Vala, GCC, G++, Node.JS a mnoho dalších..."));
		mainTerminal.AddControl(new Label(mainTerminal, 0, mainTerminal.Height - 5, "Pro více informací navštivte stránku http://www.svs.gyarab.cz"));
		mainTerminal.AddControl(new Label(mainTerminal, 0, mainTerminal.Height - 3, "Stisknutím klávesy enter ukončíte tento program..."));
		mainTerminal.AddControl(new Divider(mainTerminal, mainTerminal.Height - 3, true, true));
		mainTerminal.AddControl(new Divider(mainTerminal, mainTerminal.Width - 20, true, false));
		
		mainTerminal.Draw();
		
		getch();
		endwin();
	}

	public static void SetColours()
	{
		start_color();
		init_pair(1, Color.BLUE, Color.BLUE);
		init_pair(2, Color.BLACK, Color.WHITE);
		init_pair(3, Color.WHITE, Color.WHITE);
		bkgd(COLOR_PAIR(1));		
	}
	
	public static void ClearScreen()
	{
		erase();
		refresh();
	}
}