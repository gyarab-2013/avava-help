using Curses;

public class Control : GLib.Object
{
	public int X;
	public int Y;

	public int Width;
	public int Height;
	public Control Parent;
	
	public abstract void Draw(Window window);
}