using Curses;

public interface IContainer : GLib.Object
{
	public abstract List<Control> Controls { get; protected set; }
	
	public abstract void AddControl(Control control);
	public abstract void RemoveControl(Control control);
	public abstract bool HasControl(Control control);
}