using Curses;

public class Terminal : Control, IContainer
{
	public const int MainColour = 2;
	public const int LightColour = 3;
	
	public string Label;
	public int Width {get; protected set;}
	public int Height {get; protected set;}
	private Window _window;

	private List<Control> _controls = new List<Control>();

	public Terminal(int width, int height, int x, int y, string label = "")
	{
		_window = new Window(height, width, y, x);
		Label = label;
		Width = width;
		Height = height;
		
		SetBackground(MainColour);
		Draw();
	}

	public void Draw()
	{
		Clear();

		VerticalLine("│", 0, 0, Height, true);
		VerticalLine("│", Width - 1, 0, Height, false);
		HorizontalLine("─", 0, 0, Width, true);
		HorizontalLine("─", 0, Height - 1, Width, false);

		_window.attron(COLOR_PAIR(LightColour) | Attribute.BOLD);
		_window.mvaddstr(0, 0, "┌");
		_window.mvaddstr(Height - 1, 0, "└");
		_window.attroff(COLOR_PAIR(LightColour) | Attribute.BOLD);
		
		_window.mvaddstr(0, Width - 1, "┐");
		_window.mvaddstr(Height - 1, Width - 1, "┘");
		
		_window.mvaddstr(0, 1, Label);
		
		foreach(Control ctrl in _controls)
		{
			ctrl.Draw(_window);
		}

		_window.refresh();
	}

	public void AddControl(Control control)
	{
		_controls.append(control);
	}

	public void RemoveControl(Control control)
	{
		_controls.remove(control);
	}

	public bool HasControl(Control control)
	{
		foreach(Control ctrl in _controls)
		{
			if (ctrl == control) return true;
	    }

		return false;
	}
	
	private void HorizontalLine(string str, int x, int y, int count, bool light)
	{
		if (light) _window.attron(COLOR_PAIR(LightColour) | Attribute.BOLD);
		
		for (int i = 0; i < count; i++)
		{
			_window.mvaddstr(y, x + i, str);
		}

		if (light) _window.attroff(COLOR_PAIR(LightColour) | Attribute.BOLD);
	}
	
	private void VerticalLine(string str, int x, int y, int count, bool light)
	{
		if (light) _window.attron(COLOR_PAIR(LightColour) | Attribute.BOLD);
				
		for (int i = 0; i < count; i++)
		{
			_window.mvaddstr(y + i, x, str);	
		}

		if (light) _window.attroff(COLOR_PAIR(LightColour) | Attribute.BOLD);
	}
	
	public void SetBackground(int colour)
	{
	    _window.bkgdset(COLOR_PAIR(colour));
	}

	public void Clear()
	{
		_window.clear();
	}
}