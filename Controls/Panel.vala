using Curses;

public class Panel : Control, IContainer
{
	public List<Control> Controls { get; protected set; }

	public Panel(Control parent, int x, int y, int width, int height)
	{
		Controls = new List<Control>();
		
		Parent = parent;
		X = x;
		Y = y;
		Width = width;
		Height = height;
	}

	public void AddControl(Control control)
	{
		Controls.append(control);
	}

		public void RemoveControl(Control control)
	{
		Controls.remove(control);
	}

	public bool HasControl(Control control)
	{
		foreach(Control ctrl in Controls)
		{
			if (ctrl == control) return true;
		}

		return false;
	}

	public override void Draw(Window window)
	{
		foreach(Control ctrl in Controls)
		{
			ctrl.Draw(window);
		}
	}
}