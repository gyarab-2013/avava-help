using Curses;

public class Label : Control
{
	public string Text;
	
	public Label(Window parent, int x, int y, string str)
	{
		Parent = parent;
		X = x;
		Y = y;
		Text = str;
	}

	public override void Draw(Window window)
	{
		window.mvaddstr(Y + 1, X + 1, Text);
	}
}