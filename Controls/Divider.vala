using Curses;

public class Divider : Control
{
	public bool Light;
	public bool Horizontal;
	public const int MainColour = 2;
	public const int LightColour = 3;

	public Panel BottomPanel {get; private set;}
	public Panel TopPanel {get; private set;}
	
	public Divider(Control parent, int position, bool light, bool horizontal)
	{	
		Parent = parent;
		Horizontal = horizontal;

		if (horizontal)
		{
			X = position;
    	BottomPanel = new Panel(this, parent.X, parent.Y, parent.Width - X, parent.Height);
		TopPanel = new Panel(this, parent.X, parent.Y, X, parent.Height);
		}
		else Y = position;
		
		Light = light;
	}
	
	public override void Draw(Window window)
	{
		if (Horizontal)
		{
			for (int i = 0; i < Parent.Height; i++)
			{
				window.mvaddstr(i, X, "│");
			}
		}
		else
		{
			for (int i = 0; i < Parent.Width; i++)
			{
				window.mvaddstr(Y, i, "│");
			}
		}

		if (Light) window.attron(COLOR_PAIR(LightColour) | Attribute.BOLD);
		window.mvaddstr(0, X, "┬");
		if (Light) window.attroff(COLOR_PAIR(LightColour) | Attribute.BOLD);
		
		window.mvaddstr(Parent.Height - 1, X, "┴");

		TopPanel.Draw(window);
		BottomPanel.Draw(window);
	}
}