build:
	mkdir -p Build
	valac -o Build/AvavaHelp --pkg curses -X -lncursesw *.vala Controls/*.vala Interfaces/*.vala

run:
	./Build/AvavaHelp

clean:
	rm -rf ./Build

all: build run
